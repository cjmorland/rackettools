#!/usr/bin/env python3

# Copyright 2020 Cameron Morland

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import io

fileencoding = 'utf-8'

builtin_hofs = { 'map': 1,
                 'filter': 1,
                 'foldr': 1,
                 'foldl': 1,
                 'sort': 2,
                 'build-list': 2,
                 'apply': 1 }


def is_gracket(filename):
    '''
    Return True if the file looks like GRacket.
    '''
    with open(filename, mode='r', encoding=fileencoding) as f:

        # just look at the first line.
        for l in f:
            if l[:7] == '#reader':
                return True
            else:
                return False
    return True

def safepopstr(M):
    '''Remove str from M and return it, if not empty. Otherwise, empty string.'''
    if M: return M.pop()
    else: return ''


def tokenize_raw(L, keepcomments=False, endchar=')'):
    '''
    Note: to keep you on your toes, this moves through backwards, so you have to
    reverse them beforehand.
    '''
    def tokenize_string(L, endchar, escape_backslash=True):
        '''Special case for comments and strings'''

        R = []
        while L and L[-1] != endchar:
            R.append(L.pop())
            if R[-1] == '\\' and escape_backslash:
                # escape backslash inside string (but not comment)
                R.append(safepopstr(L))

        safepopstr(L) # throw away that endchar.

        return ''.join(R)

    R = ['']
    while L:
        # multi-line comments start with '#|' and end '|#'
        if len(L) >= 2 and L[-2:] == ['|', '#']:
            while len(L) >= 2 and L[-2:] != ['#', '|']: safepopstr(L)
            safepopstr(L)
            safepopstr(L)    

        char = safepopstr(L)

        if char == ';':       R.append(char + tokenize_string(L, endchar='\n', escape_backslash=False))
        elif char == '\\':
            # next char is escaped, so \( is just \(, not \ followed by "start child"
            R[-1] += char + safepopstr(L)

        elif char == '"':     R.append('"{}"'.format(tokenize_string(L, endchar='"')))
        elif char == endchar: break
        elif char == '(':     R.extend([tokenize_raw(L, keepcomments=keepcomments, endchar=')'), ''])
        elif char == '[':     R.extend([tokenize_raw(L, keepcomments=keepcomments, endchar=']'), ''])
        elif char == '{':     R.append([tokenize_raw(L, keepcomments=keepcomments, endchar='}'), ''])
        elif char.isspace():  R.append('') # whitespace starts a token.
        elif char == "'":
            R.append("'")
            if L:
                nextchar = L.pop()
                if nextchar == '|':
                    R[-1] += nextchar
                    while L and L[-1] != '|':
                        R[-1] += L.pop()

                    safepopstr(L)
                    R[-1] += '|'
                    R.append("")
                else:
                    L.append(nextchar) # put it back.
        # (foo'bar) means (foo 'bar)

        else:                 R[-1] += char

    def keep_item(x):
        if not isinstance(x, str): return True
        elif x == '': return False
        elif keepcomments: return True
        elif x[0] == ';': return False
        else: return True

    return list(filter(keep_item, R))

    
def tokenize(filename, keepcomments=False):
    '''
    Return an LLT representing the code in filename.
    E.g. if the file contains:
    """
    (define (foo x y) (+ 1 2 3))
    (+ (foo 2 3) 4)
    """
    it should return:
    [["define", ["foo", "x", "y"], ["+", "1", "2", "3"]],
     ["+", ["foo", "2", "3"], "4"]]
    '''

    with open(filename, mode='r', encoding=fileencoding) as f:    
        L = [x for x in "".join(f.readlines()[3:])]
        L.reverse()
        return tokenize_raw(L, keepcomments=keepcomments)

def seek(target, expr):
    '''Look for target in expr, which is a LLT of strings.'''
    for item in expr:
        if (item == target
            or (isinstance(item, list) and seek(target, item))):
            return True

    return False

def format_tree(tree, indent="  "):
    '''
    Convert the llt to a (listof Str) suitable for printing.
    '''

    if isinstance(tree, str):
        return [tree.translate({ord('\n'): '\\n'})]
    elif tree == []:
        return ['[]']
    else:
        result = []
        result.append('[')
        
        for child in tree:
            result.extend([indent + s for s in format_tree(child, indent=indent)])
        result.append(']')
        return result
    

def check_requires(tree):
    # (require "...")
    if len(tree) == 2 and tree[0] == 'require':
        return provide_functions(tree[1])
    else:
        return set()

def provide_functions(filename):
    '''
    Return the set of functions that filename makes available with `provide`.
    '''
    if filename == "racket/base":
        return set()

    try:
    
        filename = filename.strip('"')
        incltree = tokenize(filename)
        # We should also see if this file does more "require" stuff.

        provided = set()
        for child in incltree:
            if child[0] == 'provide':
                provided.update(child[1:])
            else:
                provided.update(check_requires(child))

        return provided
    
    except FileNotFoundError:
        print("Ignoring missing file: {}".format(filename))
        return set()
    
def defined(tree):
    '''
    Return a set of the names of everything defined in tree.
    '''

    funclist = set()
    for L in tree:
        if len(L) == 3:
            # (define (funcname p0 ... pn) expr)
            if L[0] == 'define' and isinstance(L[1], list):
                funclist.add(L[1][0])
            # (define funcname (... maybe lambda ...)
            elif L[0] == 'define' and isinstance(L[1], str):
                funclist.add(L[1])
            # (define-struct structname (f0 ... fn))
            elif L[0] == 'define-struct' and isinstance(L[1], str) and isinstance(L[2], list):
                struct_name = L[1]
                funclist.add("make-{}".format(struct_name))
                funclist.add("{}?".format(struct_name))
                funclist.update(["{}-{}".format(struct_name, f) for f in L[2]])
                
        funclist.update(check_requires(L))
            
    return funclist


def called(tree,
           hof = False,
           localparams = set(),
           wholefile = True):
    '''
    Return a set of the names of all functions called in tree.
    If hof is True, include functions called by map/filter/foldr, etc.    
    Functions that return functions are not handled.
    '''

    # make our own copy to prevent us from elevating things to higher level
    localparams = localparams.copy()
    
    if hof == True:
        hof=builtin_hofs
    elif not hof:
        hof = {}

    # do not evaluate the arguments to these keywords.
    ignorekeywords = {'define-struct'}
    
    result = set()
    if isinstance(tree, list):
        if tree == []: return set()
        if isinstance(tree[0], str):
            if tree[0] == 'define' and isinstance(tree[1], list):
                localparams.update(tree[1])
            
            if (tree[0] not in localparams) and (not wholefile):
                result.add(tree[0])

            # in case they are in Beginning Student* and have a function
            # named after a HOF, but fewer parameters than the index of
            # the function for the HOF, don't run off the end of the list.
            if tree[0] in hof:# and len(tree) > hof[tree[0]]:
                where = hof[tree[0]]

                if len(tree) > where:                
                    if isinstance(tree[where], str) and tree[where] not in localparams:
                        result.add(tree[where])
                    else:
                        result.update(called(tree[where], hof=hof,
                                             localparams=localparams,
                                             wholefile=False))
            elif tree[0] == 'local' and len(tree) >= 2:
                # (local   [(define ...) (define ...)]   expr)
                localparams.update(defined(tree[1]))            
        else:
            # first child is a list; call recursively inside.
            result.update(called(tree[0], hof=hof,
                                           localparams=localparams,
                                           wholefile=False))


        if (not isinstance(tree[0],str)) or (not tree[0] in ignorekeywords):
            # if the quoted flag is set, we skip the next item.
            quoted = False

            if tree[0] == "'":
                startfrom = 2
            elif tree[0] in ['lambda', 'λ']:
                startfrom = 2
                # Local parameters are also not functions, even in a lambda.
                localparams.update(tree[1])
            else: startfrom = 1

            for child in tree[startfrom:]:
                if child == "'":
                    quoted = True
                    continue

                if not quoted:
                    result.update(called(child, hof=hof,
                                                   localparams=localparams,
                                                   wholefile=False))

                quoted = False

    # these values can creep in from cond.
    return result.difference(['true', 'false', '#true', '#false', '#t', '#f']) 

def builtins(tree, hof=False):
    '''
    Return the set of functions that are called in tree, but not define in it.
    '''
    
    return called(tree, hof=hof) - defined(tree)

def recursive_functions(tree):
    '''
    Return a set of the names of all *recursive* functions defined in tree.
    Note: we do not detect *mutually-recursive* functions.
    '''

    hits = set()
    for L in tree:
        if (len(L) == 3
            and L[0] == 'define'
            and isinstance(L[1], list)):

            # This is the name of the function.
            funcname = L[1][0]
            
            # This means if they have a parameter named after their function,
            # we won't tag use of this parameter as recursion, since it isn't.
            if funcname not in L[1][1:]:
                expr = L[2]
                if seek(funcname, expr):
                    hits.add(funcname)

    return hits

    
if __name__ == '__main__':
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--dump', '-d', action='store_true',
                        help='dump the code in a standardized form.')
    parser.add_argument('--called', '-c', action='store_true',
                        help='list all called functions')
    parser.add_argument('--builtins', '-b', action='store_true',
                        help='show functions called but not defined')
    parser.add_argument('--defined', '-D', action='store_true',
                        help='show functions defined')
    parser.add_argument('--hof', '-H', action='store_true',
                        help='check params of higher order functions, map/filter/foldr/etc.')    
    
    parser.add_argument('files', nargs='+', type=str,
                        help='list of files to inspect')

    args = parser.parse_args()

    def tidy(L): return '  ' + ' '.join(sorted(L))
    
    for f in args.files:
        tree = tokenize(f)

        if args.dump:
            if args.hof: languagelevel = "htdp-intermediate-lambda-reader.ss"
            else: languagelevel = "htdp-beginner-abbr-reader.ss"
            print(';; The first three lines of this file were inserted by DrRacket. They record metadata')
            print(';; about the language level of this file in a form that our tools can easily process.')
            print('#reader(lib "{}" "lang")((read-case-sensitive #t) (htdp-settings #(#t constructor mixed-fraction #f #t none #f () #t)))'.format(languagelevel))

            for item in tree:
                print('\n'.join(format_tree(item)))

        if args.defined:
            print('{} defines:'.format(f))
            print(tidy(defined(tree)))

        if args.called:
            print('{} calls:'.format(f))
            print(tidy(called(tree, hof=args.hof)))

        if args.builtins:
            print('{} uses builtins:'.format(f))
            print(tidy(builtins(tree, hof=args.hof)))
