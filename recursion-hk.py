#!/usr/bin/env python3

# Copyright 2020 Cameron Morland

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

description = '''Print a message that shows which Racket files contain recursive
functions.  Silent for files without recursive functions.  Note: does
*not* detect mutual recursion.'''
epilog='''
To use recursively, in all subdirectories:
recursion-hk.pk `find . -name "*.rkt"`
    
To use just in subdirectories, e.g. for an assignment:
recursion-hk.py */*.rkt
'''

import rackettools
import sys
import argparse

parser = argparse.ArgumentParser(description=description, epilog=epilog,
                                 formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('files', nargs='+', type=str,
                    help='list of files to test')
args = parser.parse_args()

somehits = 0
for f in args.files:
    hits = rackettools.recursive_functions(rackettools.tokenize(f))
    if hits:
        print("*** {} contains recursive functions:".format(f))
        print('\n'.join(["    " + x for x in hits]))
        somehits = 1

exit(somehits)

        
