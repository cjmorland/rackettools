#!/usr/bin/env python3

# Copyright 2020 Cameron Morland

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import rackettools
import unittest

def reverse(s):
    return [ch for ch in s][::-1]
            
class LibraryTest(unittest.TestCase):
    def test_tokenize_simple(self):
        code = """;; (my-len loa) how long is loa? \\
(define (my-len loa)
   (cond [(empty? loa) 0]
         [else (+ 1 (my-len (rest loa)))]))

#| this is a
long long
comment. |#

(define f 5)
(define s "#|hey!|#")
"""

        tree = rackettools.tokenize_raw(reverse(code))        
        self.assertEqual(tree,
                         [['define', ['my-len', 'loa'],
                           ['cond',
                            [['empty?', 'loa'], '0'],
                            ['else', ['+', '1', ['my-len', ['rest', 'loa']]]]]],
                          ['define', 'f', '5'],
                          ['define', 's', '"#|hey!|#"']
                         ])
        self.assertEqual(rackettools.defined(tree),
                         {'f','my-len', 's'})
        self.assertEqual(rackettools.builtins(tree),
                         {'define', 'cond', 'empty?', 'else', '+', 'rest'})
        
    def test_tokenize_struct(self):
        code = """(define-struct foo (bar baz))
(define x "foo\\nbar")
"""
        tree = rackettools.tokenize_raw(reverse(code))
        self.assertEqual(tree,
                         [['define-struct', 'foo', ['bar', 'baz']],
                          ['define', 'x', '"foo\\nbar"']
                         ])
        self.assertEqual(rackettools.defined(tree),
                         {'x', 'make-foo', 'foo?', 'foo-bar', 'foo-baz'})
        self.assertEqual(rackettools.builtins(tree),
                         {'define-struct', 'define'})
        
    def test_local(self):
        code =""";; (mean-classify L) classify each in L as 'lo 'avg 'hi
(define (mean-classify L)
  (local [
          (define mean (/ (foldr + 0 L) (length L)))
          (define (classify x)
            (cond [(> x mean) 'hi]
                  [(< x mean) 'lo]
                  [else 'avg]))]
    (map classify L)))

(check-expect (mean-classify (list 4 5 6))
              (list 'lo 'avg 'hi))"""
        tree = rackettools.tokenize_raw(reverse(code))
        self.assertEqual(rackettools.defined(tree),
                         {'mean-classify'})
        self.assertEqual(rackettools.builtins(tree),
                         {'check-expect', 'local', 'define', 'cond', '<', '>', 'else', 'map', 'length', '/', 'list', 'foldr'})

        code += "(check-expect (classify 8) 'hi)"
        tree = rackettools.tokenize_raw(reverse(code))
        self.assertEqual(rackettools.builtins(tree),
                         {'check-expect', 'local', 'define', 'cond', '<', '>', 'else', 'map', 'length', '/', 'list', 'foldr', 'classify'})
                         

        
    def test_localstruct(self):
        code="""
(define (foo a b)
    (local [(define-struct CP (sum cc))
            (define x (make-CP (* 2 a)  (* 2 b)))]
      (CP-sum x)))
"""
        tree = rackettools.tokenize_raw(reverse(code))
        self.assertEqual(rackettools.builtins(tree),
                         {'define', 'local', 'define-struct', '*'})

        # now try to call a local function, and see that it appears as a builtin.
        code += "(CP-sum 5)"
        tree = rackettools.tokenize_raw(reverse(code))
        self.assertEqual(rackettools.builtins(tree),
                         {'define', 'local', 'define-struct', '*', 'CP-sum'})

    def test_quoting(self):
        code="""
'(1 2 3)
'(a b c)
'('(x))
"""
        tree = rackettools.tokenize_raw(reverse(code))
        self.assertEqual(tree,
                         [
                             "'", [ "1", "2", "3"],
                             "'", [ "a", "b", "c"],
                             "'", [ "'", [ "x" ]],
                         ])
        self.assertEqual(rackettools.builtins(tree),
                         set())
        self.assertEqual(rackettools.called(tree),
                         set())
        
    def test_params(self):
        code = """
(define (my-apply op) (op))"""

        tree = rackettools.tokenize_raw(reverse(code))
        self.assertEqual(tree,
                         [['define', ['my-apply', 'op'], ['op']]])
        self.assertEqual(rackettools.builtins(tree),
                         {'define'})

        code += "(op)"
        tree = rackettools.tokenize_raw(reverse(code))
        self.assertEqual(tree,
                         [
                             ['define', ['my-apply', 'op'], ['op']],
                             ['op']
                         ])
        self.assertEqual(rackettools.builtins(tree),
                         {'define', 'op'})
        
        
if __name__ == '__main__':
    unittest.main()
