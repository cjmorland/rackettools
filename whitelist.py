#!/usr/bin/env python3

# Copyright 2020 Cameron Morland

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import rackettools
import argparse

def chop(s, maxlen=72):
    L = []
    for i in range(0, len(s), maxlen):
        L.append(s[i:i+maxlen])

    return L
    

def scanfile(fname):
    def printfilename():
        print("In: {}: ".format(fname))
        #print("In: {}: ".format(fname[fname.rfind('/') + 1:]))

        
    try:        
        try:
            if rackettools.is_gracket(fname):
                printfilename()
                print("""This appears to be a GRacket file.
    Read this:
    https://student.cs.uwaterloo.ca/~cs135/assign/dr_racket/
    """)
                return True
            tree = rackettools.tokenize(fname)        
        except TypeError:
            printfilename()
            print("cannot parse a file.")
            return False

        try:
            forbidden = rackettools.builtins(tree, hof=enable_hofs) - allowed
        except TypeError:
            printfilename()
            print("This file appears to have syntax errors. Cannot parse.")            
            return False

        if forbidden:
            space = "\n> "
            printfilename()
            print("""The following functions are called but not defined.
If they are built-in functions, they are not permitted.{}{}\n""".format(space,
                                                                              space.join(forbidden)))
            return True
        else:
            return False
    except Exception as e:
        printfilename()
        print('Caught an unexpected exception.\nPlease report this bug.\nSaw: {}\n'.format(e))


parser = argparse.ArgumentParser()
parser.add_argument('--allowed', '-a',
                    help='filename listing allowed functions')
parser.add_argument('files',  nargs='+', type=str,
                    help='list of files to test')

args = parser.parse_args()

allowed = set({})
with open(args.allowed, "r", encoding="utf-8") as afile:
    for l in afile:
        allowed.update(l.split())

# RST can't cope with UNICODE, but we can.
if "lambda" in allowed: allowed.add("λ")

# 2-6 dots are all treated the same.
if "..." in allowed: allowed.update(['..', '...', '....', '.....', '......'])

# If we allow them to use one or more HOFs, then they must be in
# Intermediate Student or later.
enable_hofs = bool(allowed.intersection(rackettools.builtin_hofs))
        
result = list(map(scanfile, args.files))


exit(any(result))
    
