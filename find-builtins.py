#!/usr/bin/env python3

# Copyright 2020 Cameron Morland

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

description = '''

In each dir named [0-9][0-9].*, look at the file named [SRCFILE] for
lines that look like \\rfile{linerange=a-b}{filename}, or with the
--listings flag, \\lstinputlisting[...]{filename}.  

In these files, find all global functions that are called.  Print new
ones in [NEWFILE], and print all of them seen so far to [DESTFILE].


For example, if [SRCFILE] contains the following line, it will look in
that file for global functions:
    \\rfile{linerange=4-6}{code/foobar.rkt}

To omit a specific file, add % OMIT after.  So the following line will
be ignored because of the "% OMIT":
    \\rfile{linerange=4-6}{code/foobar.rkt} % OMIT

Note: if a file is referenced repeatedly, each must say "% OMIT". 
'''

import sys
import re
import rackettools
import os
import argparse

# a class to format showing defaults, but also not leave lines formatted as they are.
class CustomFormatter(argparse.ArgumentDefaultsHelpFormatter, argparse.RawDescriptionHelpFormatter):
    pass

parser = argparse.ArgumentParser(description=description,
                                 formatter_class=CustomFormatter)
parser.add_argument('--srcfile', '-s', default='beamer.tex',
                    help='LaTeX file to read in each directory') 
parser.add_argument('--newfile', '-n', default='new-functions.rkt',
                    help='Filename in which to list new functions') 
parser.add_argument('--destfile', '-d', default='permitted-functions.rkt',
                    help='Filename in which to list new functions') 
parser.add_argument('--remfile', '-r', default='removed-functions.rkt',
                    help='Filename in which to list removed functions') 

defaultignorelist = 'ignorefunctions.rkt'
parser.add_argument('--ignorelist', '-i', default=defaultignorelist,
                    help='Ignore all functions listed in this file.')
parser.add_argument('--summary', '-S', default='found-builtin-functions.rkt',
                    help='Filename to write summary to.')
parser.add_argument('--listings', '-l', action='store_true',
                    help='Look for \\lstinputlisting instead of \\rfile')
args = parser.parse_args()


def path(s):
    '''
    Return a tuple (path, filename) representing the path to s.
    path("foo/bar/bas.txt") => ("foo/bar/", "bas.txt")
    path("bas.txt") => ("./", "bas.txt")
    '''

    where = s.rfind("/")
    if where == -1: return ("./", s)
    else: return (s[:(where+1)], s[(where+1):])
    
removepa = re.compile("^[^%]*\\\\removefunctions{([^}]*)}")
if args.listings:
    rfilepa = re.compile("[^%]*\\\\lstinputlisting(\[[^\]]*\])?{([^}]*)}")
else:
    rfilepa = re.compile("[^%]*\\\\rfile({[^}]*}){([^}]*)}")

omitpa = re.compile(".*%\sOMIT")
oldbuiltins = set()

ignorefunctions = set()

if args.ignorelist:
    if os.path.isfile(args.ignorelist):
        print("Reading {}".format(args.ignorelist))
        with open(args.ignorelist, 'r') as ignorefile:
            for line in ignorefile:
                # Take everything up to the first ';' to allow comments.
                ignorefunctions.update(line.split(';',1)[0].split())
    elif args.ignorelist == defaultignorelist:
        print("Ignoring missing ignorelist {}".format(args.ignorelist))
    else:
        print("Missing ignorelist: {}".format(args.ignorelist))
        exit(1)

truecwd = os.getcwd()

moddirs = []
with os.scandir(".") as entries:
    for entry in entries:
        if entry.is_dir() and entry.name[:2].isnumeric():
            moddirs.append(entry)

moddirs.sort(key=lambda x: x.name[:2])

print("Scanning modules for '{}':".format(args.srcfile), end=' ', file=sys.stderr)

with open(args.summary, 'w') as summary:

    for mdir in moddirs:
        os.chdir(truecwd)
        os.chdir(mdir.name)

        print(mdir.name, end=' ', file=sys.stderr)
        
        scannedcodefiles = set()
        with open(args.srcfile, 'r') as src:
            called_builtins = set()
            print('\n{} adds the following functions:'.format(mdir.name), file=summary)
            texcwd = os.getcwd()
            removed = set()
            for l in src:
                os.chdir(texcwd)

                m = rfilepa.match(l)
                if m and not omitpa.match(l):
                    filename = m.group(2)
                    if filename not in scannedcodefiles:
                        (texroot, filename) = path(filename)
                        os.chdir(texroot)
                        try:
                            called_builtins.update(rackettools.builtins(rackettools.tokenize(filename)))
                        except FileNotFoundError:
                            print("Skipping missing file: {}/{}".format(texcwd,filename))

                        scannedcodefiles.add(filename)
                        
                ## now check for matches for \removefunctions{...}
                m = removepa.match(l)
                if m:                    
                    removed.update(m.group(1).split())

            newbuiltins = called_builtins - oldbuiltins - ignorefunctions

            with open(args.newfile, "w") as dest:
                print(' '.join(sorted(newbuiltins)), file=dest)

            with open(args.remfile, "w") as dest:
                print(' '.join(sorted(removed)), file=dest)

            print(' '.join(sorted(newbuiltins)), file=summary)
            oldbuiltins.update(newbuiltins)
                
            with open(args.destfile, "w") as dest:
                print(' '.join(sorted(oldbuiltins)), file=dest)


print("", file=sys.stderr)
