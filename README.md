Some tools for working with Racket files.  
* `rackettools.py` is the library, which you can `import`. You can also call it directly with various options to print information about code in Racket files.
* `find-builtis.py` is used to create lists of functions used in the notes of a course.
* `whitelist.py` takes a file containing a list of allowed functions, and any number of other files, and prints a message about each file that calls built-in functions  not in the list.
* `recursion-hk.py` takes any number of files, and prints a message identifying any recursive functions.